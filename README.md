To run the code you have to create a virtual environment
`virtualenv --system-site-packages -p python3 ./venv`

Activate it: `source venv/bin/activate`

Install requirements: `pip install -r requirements.txt`

Get image_recognition (different repository) for object detection utils:
`git clone git@gitlab.com:mainblades/image_recognition`

Install image_recognition:
`cd image_recognition && pip install .`

Now you can use the code, start either `python classify_webcam.py` or `python detect_webcam.py`.