import tensorflow as tf
import cv2
import numpy as np
from PIL import Image

from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util

# Define the video stream
cap = cv2.VideoCapture(0)  # Change only if you have more than one webcams

# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_CKPT = '/home/julian/mainblades/tf-models/ssd_mobilenet_v2_oid_v4_2018_12_12/saved_model'

# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = './oid_v4_label_map.pbtxt'

# Number of classes to detect
NUM_CLASSES = 600

# Load a (frozen) Tensorflow model into memory.
meta_graph = tf.saved_model.load(PATH_TO_CKPT)
model = meta_graph.signatures['serving_default']

# Loading label map
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(
    label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)
category_index = {key: value['name'] for key, value in category_index.items()}


def run_capture():
    while True:
        # Read frame from camera
        ret, frame = cap.read()
        img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
        img_tf_expanded = tf.expand_dims(img, axis=0)

        # Actual detection.
        output_dict = model(img_tf_expanded)
        num_detections = int(output_dict.pop('num_detections'))
        output_dict = {key: value[0, :num_detections] for key, value in output_dict.items()}
        output_dict['num_detections'] = num_detections
        # Visualization of the results of a detection.
        try:
            out_img = Image.fromarray(img)
            disp_str_list = [["{}, p={:.2f}".format(category_index[int(label)], prob)]
                             for label, prob in zip(output_dict['detection_classes'], output_dict['detection_scores'])]
            vis_util.draw_bounding_boxes_on_image(
                out_img,
                boxes=output_dict['detection_boxes'],
                display_str_list_list=disp_str_list,
                color='green',
                thickness=4)
            out_img_np = np.array(out_img)
        except Exception as e:
            print(e)
            out_img_np = img

        # Display output
        out_img_np = cv2.cvtColor(out_img_np, cv2.COLOR_RGB2BGR)
        cv2.imshow('object detection', cv2.resize(out_img_np, (800, 600)))

        if cv2.waitKey(25) & 0xFF == ord('q'):
            cv2.destroyAllWindows()
            break

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    run_capture()
