# https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_gui/py_video_display/py_video_display.html

import tensorflow as tf
import cv2
from model_loader import *

cap = cv2.VideoCapture(0)

font = cv2.FONT_HERSHEY_SIMPLEX
bottomLeftCornerOfText = (10, 460)
fontScale = 1
fontColor = (255, 255, 255)
lineType = 2

model = load_mobilenet()

resize_mode = 'rescale'
threshold = 0.3


def run_capture():
    while True:
        # Capture frame-by-frame
        ret, frame = cap.read()

        # Our operations on the frame come here
        # Preprocessing
        img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        if resize_mode == 'crop':
            img = tf.image.resize_with_crop_or_pad(img, 224, 224)
        else:
            img = tf.image.resize(img, (224, 224))
        img = tf.expand_dims(img, axis=0)
        x = densenet.preprocess_input(tf.cast(img, dtype=tf.float32))

        # ML HAPPENS HERE
        y = model.predict(x, steps=1)
        label = densenet.decode_predictions(y, top=3)[0]
        name = label[0][1]
        prob = label[0][2]

        label_text = '{} p={:.2f}'.format(name, prob) if prob > threshold else "No object found"
        cv2.putText(frame, label_text,
                    bottomLeftCornerOfText,
                    font,
                    fontScale,
                    fontColor,
                    lineType)

        # Display the resulting frame
        cv2.imshow('frame', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    run_capture()
