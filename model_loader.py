from keras.applications import mobilenet_v2, densenet


def load_mobilenet():
    model = mobilenet_v2.MobileNetV2(input_shape=None, include_top=True, weights='imagenet')
    return model


def load_densenet():
    model = densenet.DenseNet121(input_shape=None, include_top=True, weights='imagenet')
    return model


if __name__ == "__main__":
    model = load_mobilenet()
    model.summary()
